package com.example.mapper;

import com.example.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @author will.tuo
 * @date 2021/8/2 11:17
 */

@Repository
public interface UserMapper {

    User Sel(int id);
}
